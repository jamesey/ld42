extends Node2D
var scn_next_scene = preload("res://scenes/Map.tscn")

func _ready():
	var next_scene = scn_next_scene.instance()
	get_tree().get_root().add_child(next_scene)
